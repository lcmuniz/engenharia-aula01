# Engenharia de Software I 

Aula 01 - Apresentação

Prof. Luiz Carlos Melo Muniz

lcmuniz@gmail.com

---

### O Professor

- Luiz Carlos Melo Muniz
- Doutorando em Ciência da Computação (UFMA)
- Mestre em Ciência da Computação (UFMA)
- Especialista em Análise de Sistemas (UFMA)

- Email: lcmuniz@gmail.com
- Telefone: 9 9618 7238

---

### A Disciplina

- Apresentar aos alunos os principais conceitos relacionados ao ciclo de vida de desenvolvimento de software 

---

### Conteúdo Programático

- Processos de softwares e desenvolvimento ágil
- Engenharia de requistos
- Modelagem de software
- Projeto de software e arquitetura orientada a serviços
- Implementação de software e gestão de configuração
- Teste de software e evolução de software
- Reuso de software e desenvolvimento baseado em componentes

---

### A Metodologia

- Aulas teóricas e práticas
- Google Classroom
- Todo o material apresentado estará disponível *online*
- O aluno pode contactar o professor por email, Whatsapp ou Google Chat

---

### Horas e Assiduidade

- Setenta e duas horas
- Trinta e seis aulas
- Máximo de nove faltas

---

### Avaliação

- Exercícios na plataforma Google Classroom
- Duas avaliações em sala de aula
- Um trabalho em equipe a ser apresentado no final do semestre

---

### Bibliografia Básica

- **Engenharia de software**. Ian Sommerville. Editora Pearson.

- **Engenharia de Software: Teoria e Prática**. Shari Lawrence Pfleeger. Editora Pearson.

- **Desenvolvendo Software Com Uml 2.0 - Definitivo**. Ernani Medeiros. Editora Pearson.

